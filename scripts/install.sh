#!/bin/bash

#    This source file is part of the rpclient open source project
#    Copyright 2018 Ali Erkan IMREK and project authors
#    Licensed under the MIT License 








target=/var/rpclient

mkdir -p $target
mkdir -p $target/rp
mkdir -p $target/axones
mkdir -p $target/scripts
/bin/cp $target/config.ini .

/bin/cp -uRv rpclient/source/rp/* $target/rp
/bin/cp -uRv rpclient/scripts/* $target/scripts
/bin/cp -uRv rpclient/source/axones/* $target/axones
/bin/cp -uv rpclient/source/* $target

/bin/mv config.ini $target