#!/usr/bin/python
# -*- coding: utf-8 -*-




''' This source file is part of the rpclient open source project
    Copyright 2018 Ali Erkan IMREK and project authors
    Licensed under the MIT License 
'''








import platform
import subprocess
import time

from tornado import gen








class Services(object):




    #Lower value hangs the update process
    delay = 10




    def __init__(self, names=[]):
        self._last = 0
        self._names = names
        self._data = {}
        for name in names:
            self._data[name] = {"status" : "off"}
        self.update()




    @gen.coroutine
    def update(self):
        if (time.time() - self._last) < self.delay: return()
        raw = subprocess.check_output(['systemctl','-t','service', '--all'])
        lines = raw.splitlines()
        loadindex = str(lines[0]).index("LOAD")-2
        statusindex = str(lines[0]).index("SUB")-2
        descindex = str(lines[0]).index("DESCRIPTION")-2
        for line in lines:
            name = str(line[:loadindex].strip()[:-8], "utf-8")
            if name in self._names:
                status = str(line[statusindex:descindex].strip(), "utf-8")
                if status == "running": status = "on"
                else:   
                    status = "off"
                self._data[name]["status"] = status
        self._last = int(time.time())
                        



    def data(self):
        return(self._data)




    @gen.coroutine
    def change(self, name, status):
        if name in self._data.keys():
            if status or status == "true":
                self._data[name] = {"status" : "on"}
                subprocess.run(['systemctl','start', name])
            elif not status or status == "false":
                self._data[name] = {"status" : "off"}
                subprocess.run(['systemctl','stop', name])
        self._last = int(time.time())



    @gen.coroutine
    def cmd(self, cmd):
        try:
            for name in cmd["data"].keys():
                self.change(name, cmd["data"][name])
            return(True)
        except:
            return(False)
