#!/usr/bin/python
# -*- coding: utf-8 -*-




''' This source file is part of the rpclient open source project
    Copyright 2018 Ali Erkan IMREK and project authors
    Licensed under the MIT License 
'''








import sys
import os
import random
import time
spath = os.path.dirname(os.path.realpath(__file__))
if spath not in sys.path:    sys.path.append(spath)

from rp.main import RPMain, Run
from axones.nixsys.main import *
from axones.services.main import *








class MyNode(RPMain):
	



    #Initializing
    def prepare(self):
        self.sys = NixSys()
        self.srv = Services(["ssh", "nginx"])
        
        #It can be change depends on cpu usage
        self.wheel_sleep = 0.00001




    #Ready for update
    def ready(self):
        #Get server task names if need
        self.sensor1 = self.task_data("test")




    #Runs after login as a thread
    def wheel(self):        
        while True:
            self.sensor1["test_value"] = random.randrange(100)
            self.update_task("test", self.sensor1)
            time.sleep(self.wheel_sleep)




    #Updates data before every up
    def pre_update(self):

        self.sys.update()
        self.update_task("system", self.sys.data())

        self.srv.update()
        self.update_task("service", self.srv.data())

        


    #Runs after every up, so messages contains commands and followed tasks data
    def post_update(self, cmds, tasks):
        if cmds:
            print("cmd : ", cmds)
        if tasks:
            print("tasks : ", tasks)








Run(MyNode)