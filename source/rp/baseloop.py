#!/usr/bin/python
# -*- coding: utf-8 -*-




''' This source file is part of the rpclient open source project
    Copyright 2018 Ali Erkan IMREK and project authors
    Licensed under the MIT License 
'''








import time
from tornado import ioloop







class BaseLoop(object):




	min = 2000




	def __init__(self, callback, loop):
		self._callback = callback
		self.looptime = self.min
		self._starttime = 0
		self._endtime = 0
		self.delay = 0
		self._period = ioloop.PeriodicCallback( 
					self._back,
					self.looptime)
		



	def start(self):
		self._starttime = 0
		self._endtime = 0
		self.looptime = self.min
		self._period.callback_time = self.looptime
		self._period.start()




	def _back(self):
		if self._starttime > 0:
			self.looptime = (((self._endtime - self._starttime)+self.delay)*1000)+self.min
		if self.looptime == 0:	self.looptime = self.min
		self._period.callback_time = self.looptime
		self._starttime = int(time.time())
		self._callback()




	def stop(self):
		self._period.stop()




	def endtiming(self):
		self._endtime = int(time.time())


	def is_running(self):
		return(self._period.is_running())