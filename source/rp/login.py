#!/usr/bin/python
# -*- coding: utf-8 -*-




''' This source file is part of the rpclient open source project
    Copyright 2018 Ali Erkan IMREK and project authors
    Licensed under the MIT License 
'''








import json

from .baseloop import BaseLoop









class LoginTask(BaseLoop):



    def __init__(self, succesback, loop):
        super(LoginTask, self).__init__(self._try, loop)
        self._succeed = succesback
        self._running = False
        self._loop = loop
        self._try_count = 0

        


    def _try(self):
        if self._running == False:
            self._running = True
            self._loop.rp_client.login(self._response)




    def _response(self, result, msg):
        if result:
            self._try_count = 0
            self.delay = 0
            if self.check_version(msg):
                self._succeed(msg)
                self._try_count = 0
                self.stop()
            else:
                quit()
        else:
            self._try_count += 1
            self.delay = self._try_count
            if "HTTP" in msg and self._try_count < 60:
                self._loop.rp_log.error("Server or connection error: %s", msg)
            else:
                self._loop.rp_log.error("User authentication failed: %s", msg)
                self.delay = 120
        self.endtiming()
        self._running = False




    def check_version(self, msg):
        for s in msg["stack"]:
            if "subver" in s["data"]:
                ver = self._loop.rp_conf['SERVER']['server_ver']
                subver = self._loop.rp_conf['SERVER']['server_subver'].split(",")
                if ver == s["data"]["ver"] and s["data"]["subver"] in subver:
                    return(True)
        self._loop.rp_log.error("Unsupported server version: {}.{}".format(s["data"]["ver"],s["data"]["subver"]))
        return(False)

