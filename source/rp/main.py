#!/usr/bin/python
# -*- coding: utf-8 -*-




''' This source file is part of the rpclient open source project
    Copyright 2018 Ali Erkan IMREK and project authors
    Licensed under the MIT License 
'''








import sys
import time
import json
import configparser
import logging
from logging.handlers import RotatingFileHandler
from logging import handlers

from tornado import httpclient, ioloop, gen
from concurrent.futures import ThreadPoolExecutor

from .msg import *
from .utils import get_uri
from .server import Client
from .baseloop import BaseLoop
from .login import LoginTask
from .ping import PingTask








LOG_LEVELS={
    "CRITICAL":50,
    "ERROR":40,
    "WARNING":30,
    "INFO":20,
    "DEBUG":10,
    "NOTSET":0}





thread_pool = ThreadPoolExecutor(1)








def Run(handler):
    if handler.__module__ == "__main__":
        try:
            conf = configparser.ConfigParser()
            conf.read('config.ini')

            #Add Compatible server versions
            conf['SERVER']['server_ver'] = "OMM"
            conf['SERVER']['server_subver'] = "1"

            log = logging.getLogger('rp')
            log.setLevel(LOG_LEVELS[conf['CLIENT']['log_level']])
            format = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
            fh = handlers.RotatingFileHandler(conf['CLIENT']['log_file'], maxBytes=(1048576*5), backupCount=7)
            fh.setFormatter(format)
            log.addHandler(fh)
        except Exception as inst:
            print(inst.args)
            raise
        mainloop = ioloop.IOLoop.instance()
        mainloop.rp_conf = conf
        mainloop.rp_log = log
        mainloop.rp_client = Client(conf)
        application = handler(mainloop)
        log.info("Starting...")
        mainloop.start()








class RPMain(object):




    coroutine = gen.coroutine


    def __init__(self, mainloop):
        self.__stack = Stack()
        self.__loop = mainloop
        self.__login = LoginTask(self.__login_succeed, mainloop)
        self.__ping = PingTask(self.__ping_succeed, self.__ping_failed, mainloop)
        self.__up = BaseLoop(self.__try_up, mainloop)
        self.__loop.handle_callback_exception = self.__error
        self.__loop.rp_log.debug("Preparing...")
        self._up_running = False
        self.prepare()
        self.__loop.rp_log.info("Login...")
        self.__login.start()




    @gen.coroutine
    def __run_wheel(self):
        thread_pool = ThreadPoolExecutor(1)
        yield thread_pool.submit(self.wheel)




    def __error(self):
        import sys
        (typ, value, traceback) = sys.exc_info()
        self.__loop.rp_log.error("%s - %s", value, str(traceback))




    def __login_succeed(self, msg):
        stack = Stack()
        stack.load(msg["stack"])
        tasklist = self.__get_command("tasklist", stack)
        self.__loop.rp_log.info("Logged in")
        for task in tasklist:
            self.__loop.rp_log.debug("Task appending : %s", task["name"])
            self.__append_task(name=task["name"], id=task["id"])
            self.ready()
        self.__loop.rp_log.debug("Wheel rolling")
        self.__run_wheel()
        self.__loop.rp_log.info("Starting ping...")
        self.__ping.start()




    def __get_command(self, name, stack):
        uri = get_uri(
            self.__loop.rp_conf["SERVER"]["server_name"], 
            self.__loop.rp_conf["SERVER"]["server_node"], 
            "command")
        if name in stack.data(uri):
            return(stack.data(uri)[name])
        else:
            return(False)




    def __login_failed(self):
        self.__loop.rp_log.error("Login failed")




    def __ping_succeed(self, msg):
        stack = Stack()
        stack.load(msg["stack"])
        cmd = self.__get_command("ping", stack)
        if cmd == "awake":
            self.__loop.rp_log.info("Waking up, starting update...")
            self.__ping.stop()
            self.__up.start()




    def __ping_failed(self):
        self.__loop.rp_log.info("Try to login again")
        self.__ping.stop()
        self.__login.start()




    @gen.coroutine
    def __try_up(self):
        if self._up_running == False:
            self._up_running = True
        self.pre_update()
        msg = Message( 
            uname = self.__loop.rp_conf["USER"]["uname"], 
            nname = self.__loop.rp_conf["USER"]["nodename"],
            stack = self.__stack)
        dump = msg.dump
        self.__loop.rp_log.debug("Updating")#, appendix = jdump(dump))
        self.__loop.rp_client.update(self.__up_response, dump)




    def __up_response(self, result, stack):
        if result:
            cmd , tasks = self.__parse_reply(stack.stack)
            self.__loop.rp_log.debug("Update succesful")
            self.post_update(cmd, tasks)
            self.__up.endtiming()
            if self.__get_command("ping", stack) == "sleep":
                self.__loop.rp_log.info("Going to sleep")
                self.__up.stop()
                self.__ping.start()
        else:
            self.__up.stop()
            self.__ping.start()
        self._up_running = False

        


    def prepare(self):
        pass



    def ready(self):
        pass



    def pre_update(self):
        pass




    def post_update(self):
        pass




    def wheel(self):
        pass
            



    def __append_task(self, name, data={}, id=""):
        self.__stack.append({ 
            "uname": self.__loop.rp_conf["USER"]["uname"], 
            "nname": self.__loop.rp_conf["USER"]["nodename"],
            "name": name, 
            "id": id},
            data)




    def update_task(self, name, data):
        uri = get_uri(
            self.__loop.rp_conf["USER"]["uname"], 
            self.__loop.rp_conf["USER"]["nodename"],
            name)
        self.__stack.update(uri, data)




    def task_data(self, name):
        uri = get_uri(
            self.__loop.rp_conf["USER"]["uname"], 
            self.__loop.rp_conf["USER"]["nodename"],
            name)
        return(self.__stack.data(uri))




    def __delete_task(self, name):
        uri = get_uri(
            self.__loop.rp_conf["USER"]["uname"], 
            self.__loop.rp_conf["USER"]["nodename"],
            name)
        self.__stack.delete(uri)




    def __parse_reply(self, stack):
        cmd = []
        tasks = []
        for msg in stack:
            if msg["name"] == "command":
                if "admincommand" in msg["data"].keys():
                    cmd = msg["data"]["admincommand"]
            else:
                tasks.append(msg)
        return(cmd, tasks)



