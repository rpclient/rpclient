#!/usr/bin/python
# -*- coding: utf-8 -*-




''' This source file is part of the rpclient open source project
    Copyright 2018 Ali Erkan IMREK and project authors
    Licensed under the MIT License 
'''







from .baseloop import BaseLoop








class PingTask(BaseLoop):



    def __init__(self, succesback, failback, loop):
        super(PingTask, self).__init__(self._try, loop)
        self._succeed = succesback
        self._failed = failback
        self._running = False
        self._loop = loop




    def _try(self):
        self._loop.rp_log.debug("Ping")
        if self._running == False:
            self._running = True
            self._loop.rp_client.ping(self._response)




    def _response(self, result, body=""):
        if result:
            self._succeed(body)
        else:
            self._loop.rp_log.error("Ping failed: %s", str(body))
            self._failed()
        self.endtiming()
        self._running = False